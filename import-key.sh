#!/bin/bash

CERT=$(ls -dt $PWD/*.cer | head -1)
CERTNAME=$(ls -t *.cer | head -1 | sed "s:.cer::")
JAVADIR=$(readlink -f /usr/bin/java | sed "s:bin/java::")

cd $JAVADIR/bin
sudo ./keytool -importcert -trustcacerts -alias $CERTNAME -file $CERT -storepass changeit -noprompt -keystore $JAVADIR/lib/security/cacerts
