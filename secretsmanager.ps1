java -jar certgrabber.jar https://secretsmanager.us-east-1.amazonaws.com
$certificates = Get-ChildItem . -Filter *.cer | Sort-Object -Descending -Property LastWriteTime
$certificate = $certificates[0]
$certName = $certificate.BaseName
$path = $certificate.FullName

$jdk11s = Get-ChildItem -Filter "jdk-11.0*" -Path "$env:USERPROFILE\Programs"
foreach ($jdk in $jdk11s) {
    $fullName = $jdk.FullName
    & "$fullName\bin\keytool.exe" "-importcert" "-trustcacerts" "-alias" "$certName" "-file" "$path" "-storepass" "changeit" "-noprompt" "-keystore" "$fullName\lib\security\cacerts"
}

if (Test-Path -Path "$env:USERPROFILE\Programs\jdk8\bin\keytool.exe") {
    & "$env:USERPROFILE\Programs\jdk8\bin\keytool.exe" "-importcert" "-trustcacerts" "-alias" "$certName" "-file" "$path" "-storepass" "changeit" "-noprompt" "-keystore" "$env:USERPROFILE\Programs\jdk8\jre\lib\security\cacerts"
}

Write-Host "Please restart all instances of IntelliJ to pick up the change"