package com.argus.certgrabber;

import nl.altindag.ssl.util.CertificateUtils;

import java.io.IOException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.Map;

public class Main {

    public static void main(String[] args) throws IOException {
        if (args.length == 0) {
            System.out.println("Usage: java -jar certgrabber.jar <target url>");
            return;
        }

        Map<String, List<String>> certificateAsPem = CertificateUtils.getCertificateAsPem(args[0]);

        URL url = new URL(args[0]);
        List<String> list = certificateAsPem.get(args[0]);

        Path path = Paths.get(url.getHost() + ".cer");
        int counter = 1;
        while (Files.exists(path)) {
            path = Paths.get(url.getHost() + "-" + counter + ".cer");
            counter++;
        }

        Files.write(path, String.join("\n", list).getBytes());
    }
}
