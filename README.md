# README #

Aflac laptops come pre-installed with something called Zscaler, which is a man-in-the-middle attack that Aflac performs
on all HTTPS traffic coming out of your laptop so they know what you're looking at and typing into websites. Zscaler intercepts the
HTTPS traffic, and rewrites the server certificates so they look like they're coming from Zscaler. Most applications
that use the built-in Windows certificate stores will have no problems, as the Zscaler certificate has been pre-deployed
there, however applications with their own certificate stores, such as Java or Python, will have problems as they do not 
trust the Zscaler root cert.

After much struggle, this repo and script have been prepared, so that you can get the server response that Java sees
after Zscaler has manipulated it, and store the certificates that Zscaler substituted back into Java's cacerts, which
will then resolve the issue.


### Specific Usages ###
Two utility scripts have been provided for the most common use cases: refreshing Gradle and AWS Secrets Manager certs.
I say refreshing because zscaler-issued certs normally are only valid for about 2 weeks. Simply run gradle.ps1 or 
secretsmanager.ps1 to perform the refresh, or initial import as the case may be; these scripts have the necessary URLs 
baked into them already so that all that needs to be done is simply to invoke the script.


### General Usage instructions ###
(In Powershell)

* import-key.ps1 &lt;target url&gt;
* Disregard the warning import-key.ps1 spits out

### Powershell Note ###
Helpfully, Aflac has also in their infinite wisdom tried to block Powershell scripts, thankfully however Microsoft does 
not truly allow this. To get around blocked script messages, you may wish to create a shortcut to Powershell and set 
the following as the target:

* %SystemRoot%\system32\WindowsPowerShell\v1.0\powershell.exe -ExecutionPolicy ByPass

The "-ExecutionPolicy ByPass" arguments tell Powershell to ignore Aflac's trifling efforts to block scripts, enterprises
do love the security theater of having settings to toggle, and Microsoft satisfies this deep insecurity on their part
while also allowing power users to get around it easily. Shhh, don't tell. :)

### Intellij & Gradle Note ###
Lastly, if going through all this rigmarole to get Gradle to work in IntelliJ, after completing the certificate import
process, you will need to restart all IntelliJ windows with Gradle projects in them. Merely telling IntelliJ to
refresh the project won't work. Lovely. Anyways, that about covers it all.